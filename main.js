'use strict';
let eyeParents = document.getElementsByClassName('eye-parent');
let arrayOfEyes = Array.from(eyeParents);
arrayOfEyes.forEach(onEyeClick);

function onEyeClick(item) {                    // заставляю глаз мигать
  item.addEventListener('click', function (event) {
    let i = event.target.closest('i');
    if (i == null) return;
    i.classList.toggle('fa-eye-slash');
  })
}



let form = document.forms.form;              // привязываю глаз к паролю
let elements = document.form.elements.pass;

let input = Array.from(elements);

input.forEach(changePassword);
function changePassword(item) {

  item.addEventListener('click', function () {
    if (item.getAttribute('type') === 'password') {
      item.setAttribute('type', 'text')
    }
    else {
      item.setAttribute('type', 'password');
    }
  })
};


let btn = document.querySelector('.btn');         
btn.addEventListener('click', checkPassword);

function checkPassword(event) {                                                       //  пошла проверка паролей
  event.preventDefault();
  removeMessage();                       // сделала один вывод сообщения об ошибке

  if (input[0].value === '' && input[1].value === '') {
    alert('Вы не ввели пароль');
  }

  if (input[0].value !== input[1].value) {
    let message = document.createElement('div');
    message.className = 'message';
    message.innerHTML = 'Введите одинаковые значения';
    message.style.cssText = `color: red;
        font-weight: bold;
        margin-top: 0;
        margin-bottom:20px;
        text-align: center;
        display: block;
        `
    input[1].after(message);
  }

  if (input[0].value !== '' && input[1].value !== '' && input[0].value === input[1].value) {
    alert('Добро пожаловать!');
    input.forEach((item) => item.value = '');
  }

}

function removeMessage() {
  let error = document.getElementsByClassName('message');
  for (let i = 0; i < error.length; i++) {
    error[i].remove();
  }
}


input.forEach(deleteMes);              // убираю сообщение об ошибке, если попытка изменить пароль
function deleteMes(item) {
  item.addEventListener('focus', function () {
    let mess = document.querySelector('.message');
    if (mess) {
      mess.innerHTML = "";
    }
  })
}







